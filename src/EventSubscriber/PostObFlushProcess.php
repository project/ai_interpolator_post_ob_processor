<?php

namespace Drupal\ai_interpolator_post_ob_processor\EventSubscriber;

use Drupal\ai_interpolator\AiInterpolatorStatusField;
use Drupal\ai_interpolator_post_ob_processor\Plugin\AiInterpolatorProcess\PostObProcessor;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * We run some code after response is given back.
 */
class PostObFlushProcess implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::TERMINATE => 'onTerminate'];
  }

  /**
   * Code to be executed after the response is sent.
   */
  public function onTerminate(TerminateEvent $event) {
    // Check if something should change.
    if ($event->getRequest()->get(PostObProcessor::REQUEST_KEY, FALSE)) {
      // Run all fields with this rule.
      _ai_interpolator_entity_can_save_toggle(FALSE);
      foreach ($event->getRequest()->get(PostObProcessor::QUEUE_KEY, []) as $field) {
        $field['entity'] = \Drupal::service('ai_interpolator.rule_runner')->generateResponse($field['entity'], $field['fieldDefinition'], $field['interpolatorConfig']);

      }
      // Set processing done.
      $field['entity']->set(AiInterpolatorStatusField::FIELD_NAME, AiInterpolatorStatusField::STATUS_FINISHED);
      $field['entity']->save();
      _ai_interpolator_entity_can_save_toggle(TRUE);
    }
  }

}
