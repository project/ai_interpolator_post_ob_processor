<?php

namespace Drupal\ai_interpolator_post_ob_processor\Plugin\AiInterpolatorProcess;

use Drupal\ai_interpolator\AiInterpolatorStatusField;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldProcessInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The post output buffer processor.
 *
 * @AiInterpolatorProcessRule(
 *   id = "post_ob",
 *   title = @Translation("Post Output Buffer"),
 *   description = @Translation("Saves the interpolation after giving back a response in the browser. Good for API's."),
 * )
 */
class PostObProcessor implements AiInterpolatorFieldProcessInterface, ContainerFactoryPluginInterface {

  /**
   * The request key status.
   */
  const REQUEST_KEY = 'ai_interpolator_post_ob_processor_enabled';

  /**
   * The request key status.
   */
  const QUEUE_KEY = 'ai_interpolator_post_ob_processor_queue';

  /**
   * A queue factory.
   */
  protected RequestStack $requestStack;

  /**
   * Constructor.
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('request_stack'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function modify(EntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    // Add to the queue of fields.
    $currentQueue = $this->requestStack->getMainRequest()->attributes->get(self::QUEUE_KEY, []);
    $currentQueue[] = [
      'entity' => $entity,
      'fieldDefinition' => $fieldDefinition,
      'interpolatorConfig' => $interpolatorConfig,
    ];
    $this->requestStack->getMainRequest()->attributes->set(self::QUEUE_KEY, $currentQueue);
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function preProcessing(EntityInterface $entity) {
    $entity->ai_interpolator_status = AiInterpolatorStatusField::STATUS_PROCESSING;
    // Set that we want to modify something.
    $this->requestStack->getMainRequest()->attributes->set(self::REQUEST_KEY, TRUE);
  }

  /**
   * {@inheritDoc}
   */
  public function postProcessing(EntityInterface $entity) {
  }

}
